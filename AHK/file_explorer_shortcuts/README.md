# Window Explorer Shortcuts

Keyboard shortcuts:

**In a Windows Explorer Folder:**

<kbd>WIN</kbd> + <kbd>Enter</kbd> - Open command line in this directory.



**Anywhere:**

<kbd>WIN</kbd> + <kbd>Enter</kbd> - Open command line in starting directory (See settings.ini).

<kbd>WIN</kbd> + <kbd>E</kbd> - new Windows Explorer window with focus on Favorites

<kbd>CTRL</kbd> + <kbd>F4</kbd> - Close ALL Windows Explorer Windows

