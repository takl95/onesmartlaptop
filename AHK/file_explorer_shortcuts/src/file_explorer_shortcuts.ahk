﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force ; Ensures only one running instance at a time.

;# Win key
;+ shift key
;! alt key
;^ ctrl key


#IfWinactive ahk_class CabinetWClass

#Enter::
Send, !D
Send, cmd
Send {Enter}
return

#IfWinActive

#E::
Run,explorer
WinWaitActive ahk_class CabinetWClass
Send {Esc}
Send, +{tab}
Send {Home}
return

#Enter::Run, cmd, D:\laciapps
^F4::	Run, nircmd.exe win close class "CabinetWClass"