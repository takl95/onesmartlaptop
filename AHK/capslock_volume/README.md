# Capslock Volume

Keyboard shortcuts:

<kbd>WIN</kbd> + <kbd>Capslock</kbd> - 100% - max volume.

<kbd>SHIFT</kbd> + <kbd>Capslock</kbd> - set volume to 80%

<kbd>ALT</kbd> + <kbd>Capslock</kbd> - set volume to 50%

<kbd>CTRL</kbd> + <kbd>Capslock</kbd> - set volume to 25%

<kbd>Capslock</kbd> - set volume to 0% (mute*)



*Not actually muted for convenience, volume just set to 0