﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force ; Ensures only one running instance at a time.

;# Win key
;+ shift key
;! alt key
;^ ctrl key

CapsLock::	Run, nircmd.exe setsysvolume 0  ; muting but not really
#CapsLock::	Run, nircmd.exe setsysvolume 65535 ; as loud as it gets
+CapsLock::	Run, nircmd.exe setsysvolume 52428 ; 80%
!CapsLock::	Run, nircmd.exe setsysvolume 32717 ; 50%
^CapsLock::	Run, nircmd.exe setsysvolume 16384 ; 25%